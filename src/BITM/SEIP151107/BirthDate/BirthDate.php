<?php
namespace App\BirthDate;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class BirthDate extends DB{
    public $id = "";
    public $name = "";
    public $date = "";
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($data=NULL){
        if(array_key_exists("id",$data)){
            $this->id=$data["id"];
        }
        if(array_key_exists("name",$data)){
            $this->name=$data["name"];
        }
        if(array_key_exists("date",$data)){
            $this->birthday=$data["date"];
        }
    }
    public function store(){
        $dbh=$this->connection;
        $values=array($this->name,$this->date);
        $query="insert into birthday(name,date) VALUES (?,?)";
        $sth=$dbh->prepare($query);
        $result= $sth->execute($values);

        if($result){
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ birthday: $this->date ] <br> Data Has Been Inserted Successfully!</h3></div>");
        }else{
            Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ birthday: $this->date ] <br> Data Has Not Been Inserted Successfully!</h3></div>");
        }

        Utility::redirect('create.php');


    }
}

