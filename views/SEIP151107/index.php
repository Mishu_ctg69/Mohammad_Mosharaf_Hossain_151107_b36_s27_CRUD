<head>
    <title>Atomic Project</title>
    <link rel="stylesheet" href="resource/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="resource/Bootstrap/css/style.css">
    <link media="all" type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body
        {
            background-image: url(resource/img/bg4.jpg);
            background-size:cover;

            }
    </style>

</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-3 sidebar2">
            <div class="logo text-center">
                <img src="resource/img/logo.jpg" class="image" alt="Logo">
                <h4>M.M.H Mishu</h4>

                <div>
                    <button type="button" class="btn btn-default Add-friend">
                        <i class="fa fa-rocket" aria-hidden="true"></i> LAUNCH A CHALLENGE
                    </button>
                </div>

            </div>
            <div>

                <ul class="list">
                    <li><i class="fa fa-book" aria-hidden="true"></i><a href="BookTitle/create.php">Booktitle</a></li>
                    <li><i class="fa fa-birthday-cake" aria-hidden="true"></i><a href="Birthday/create.php">Birthday</a></li>
                    <li><i class="fa fa-building-o" aria-hidden="true"></i><a href="City/create.php">  City</a></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="Email/create.php">Email</a></li>
                    <li><i class="fa fa-venus-mars" aria-hidden="true"></i><a href="Gender/create.php">Gender</a></li>
                    <li><i class="fa fa-bicycle" aria-hidden="true"></i><a href="Hobbies/create.php">Hobbies</a></li>
                    <li><i class="fa fa-photo" aria-hidden="true"></i><a href="ProfilePicture/create.php">Profile Picture</a></li>
                    <li><i class="fa fa-thumbs-up" aria-hidden="true"></i><a href="SummaryOfOrganization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>
<!--        <div align="center" >-->
<!--            <h1> Atomic Project !!! </h1>-->
<!--        </div>-->
    </div>
</div>
</body>