<?php

require_once("../../../vendor/autoload.php");

use App\Message\Message;
use App\BookTitle\BookTitle;

$objBooktitle = new BookTitle();
$objBooktitle->setData($_GET);

$oneData = $objBooktitle->view("obj");

//var_dump($oneData);die;
//echo Message::getMessage();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Booktitle</title>
    <link rel="stylesheet" href="../resource/css/bootstrap.min.css">
    <script src="../resource/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../resource/Bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../resource/Bootstrap/css/style.css">
    <link media="all" type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        body{
            padding-top: 20px;
            background-color: #0f0f0f;
            background: url("../resource/img/bg1.jpg") no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }
    </style>

</head>

<body>
<div class="container" class="container-fluid">
    <div class="row">
        <div class="col-md-3 col-sm-3 sidebar2" style="margin-left: -50px;">
            <div class="logo text-center">
                <img src="../resource/img/logo.jpg" class="image" alt="Logo">
                <h4>M.M.H Mishu</h4>

                <div>
                    <button type="button" class="btn btn-default Add-friend">
                        <i class="fa fa-rocket" aria-hidden="true"></i> LAUNCH A CHALLENGE
                    </button>
                </div>

            </div>
            <div>

                <ul class="list">
                    <li><i class="fa fa-book" aria-hidden="true"></i><a href="../BookTitle/create.php">Booktitle</a></li>
                    <li><i class="fa fa-birthday-cake" aria-hidden="true"></i><a href="../Birthday/create.php">Birthday</a></li>
                    <li><i class="fa fa-building-o" aria-hidden="true"></i><a href="../City/create.php">  City</a></li>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="../Email/create.php">Email</a></li>
                    <li><i class="fa fa-venus-mars" aria-hidden="true"></i><a href="../Gender/create.php">Gender</a></li>
                    <li><i class="fa fa-bicycle" aria-hidden="true"></i><a href="../Hobbies/create.php">Hobbies</a></li>
                    <li><i class="fa fa-photo" aria-hidden="true"></i><a href="../ProfilePicture/create.php">Profile Picture</a></li>
                    <li><i class="fa fa-thumbs-up" aria-hidden="true"></i><a href="../SummaryOfOrganization/create.php">Summary of Organization</a></li>
                </ul>
            </div>
        </div>
        <div class="row centered-form text-center" style="margin-top: 18%; margin-right: 12%">

            <!--        <h2 style="color: #a6e1ec">Create.php</h2>-->
            <div style="width: 500px" align="center" class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit Book Title</h3>
                    </div>
                    <p style="color: #31b0d5;text-align: center">
                        <?php
                        echo Message::message();
                        ?>
                    </p>
                    <div class="panel-body">
                        <form  role="form" action="update.php" method="post">
                            <div class="form-group">
                                <label for="tt"></label>
                                <input type="text" name="book_title" id="first_name" class="form-control input-sm" value="<?php echo $oneData->book_title ?>">
                            </div>
                            <div class="form-group">
                                <input type="text" name="author_name" id="email" class="form-control input-sm" value="<?php echo $oneData->author_name ?> ">
                            </div>
                            <input type="submit" value="Update" class="btn btn-info btn-block">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
